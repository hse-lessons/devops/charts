FROM alpine:3.14

ENV KUBE_VERSION="1.22.0"

# Install kubectl
RUN apk add --no-cache ca-certificates bash curl && \
    curl -L -o /usr/local/bin/kubectl https://storage.googleapis.com/kubernetes-release/release/v${KUBE_VERSION}/bin/linux/amd64/kubectl && \
    chmod +x /usr/local/bin/kubectl

# Install Helm
RUN apk add --no-cache openssl && \
    curl https://get.helm.sh/helm-v3.7.2-linux-amd64.tar.gz | tar zx && \
    mv linux-amd64/helm /usr/local/bin/helm && \
    chmod +x /usr/local/bin/helm
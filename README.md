**Автодеплой Helm-чартов в кластер Kubernetes при помощи CI/CD. Гайд**

Для настройки автодеплоя ранее созданых Helm-чартов в кластер, нам необходимо создать Docker образ с предустановленными Helm, kubectl и yq на борту.
Пропишем для этого следующие параметры:

```Dockerfile
FROM alpine:3.14

ENV KUBE_VERSION="1.22.0"

# Install kubectl
RUN apk add --no-cache ca-certificates bash curl && \
    curl -L -o /usr/local/bin/kubectl https://storage.googleapis.com/kubernetes-release/release/v${KUBE_VERSION}/bin/linux/amd64/kubectl && \
    chmod +x /usr/local/bin/kubectl

# Install Helm
RUN apk add --no-cache openssl && \
    curl https://get.helm.sh/helm-v3.7.2-linux-amd64.tar.gz | tar zx && \
    mv linux-amd64/helm /usr/local/bin/helm && \
    chmod +x /usr/local/bin/helm
```

Соберите и загрузите Docker образ в наш Container Registry на gitlab.com. Например:

```bash
docker build -t helm
docker tag helm gitlab.com/teta-students/hse_andropov/devops/images/helm:latest
docker push gitlab.com/teta-students/hse_andropov/devops/images/helm:latest
```

Проверьте наличие ранее загруженно образа в Container Registry в Вашем GitLab репозитории по следующему пути: HSE_Student/DevOps/Images.
При желании и для быстрой актуализации образа, рекомендую написать CI/CD для сборки образа в репозитории DevOps/Images.

Следующим шагом нам понадобится Ваш KUBECONFIG (файл конфигурации с параметрами для подключения к кластеру). Требуется добавить этот файл в CI/CD Variables нашего проекта.
Назовем его kube_config. Добавьте его в незакодированном виде, чуть позже придется закодировать методом base64.

Также можно сразу закодировать его вручную, и добавить значения выходного файла в CI/CD Variables.

```bash
base64 -w 0 path/to/your/kubeconfig > kbase64.txt
```

Теперь обновите переменную kube_config в настройках CI/CD вашего проекта GitLab (Настройки > CI/CD > Переменные), указав содержимое файла kbase64.txt.

```yaml
image: docker.io/persikida/helm:latest

variables:
  NAMESPACE: core
  RELEASE: hse
  KUBECONFIG: /root/.kube/config

stages:
  - deploy_charts

deploy_charts_dev:
  resource_group: development
  extends: .deploy
  allow_failure: true

.deploy:
  stage: deploy_charts
  before_script:
    # Настройка KUBECONFIG
    - mkdir -p "${KUBECONFIG%/*}" && touch "$KUBECONFIG"
    - echo $kube_config | base64 -d > ${KUBECONFIG}
    - chmod 400 $KUBECONFIG
    # Выводим информацию о кластере, счтобы удостовериться в работоспособности подключения
    - kubectl cluster-info

  script: 
    # Выводим список подов в подгруппе core
    - kubectl -n $NAMESPACE get po
    # Получаем версию Helm
    - helm version
    # В нашем репозитории должна быть главная директория, в которой будем хранить наши чарты. Назовем ее 'hse'
    - _MAIN_CHARTS_DIR=hse
    # Подкаталог с чартами
    - _SUBCHARTS_DIR=$_MAIN_CHARTS_DIR/charts
    # Осуществляем проверку ветки. Если ветка prod - берем значения чартов из values-prod.yml, в ином случае - values.yml
    - |
      if [ "$CI_COMMIT_REF_NAME" = "prod" ]; then
        _PROJECTS=$(yq eval '.tags | with_entries(select(.value == true)) | keys | .[]' values-prod.yml)
      else
        _PROJECTS=$(yq eval '.tags | with_entries(select(.value == true)) | keys | .[]' values.yml)
      fi
    # Перебираем массив проектов циклом for из values.yml и проверяем наличие values для текущего проекта в директории с подчартами
    # В ином случае переменная _VALUES очищается. Если все тип-топ - выполняем установку/обновление чартов в нашем кластере
    - |
      for project in ${_PROJECTS[@]}; do
          test -f $_SUBCHARTS_DIR/$project/values-${ENV}.yml &&
            _VALUES="-f $_SUBCHARTS_DIR/$project/values.yml -f $_SUBCHARTS_DIR/$project/values-${ENV}.yml" ||
            unset _VALUES
          helm upgrade --dry-run --install --namespace $NAMESPACE $project $_SUBCHARTS_DIR/$project/ $_VALUES
          helm upgrade --install --wait --timeout 1m30s --namespace $NAMESPACE $project $_SUBCHARTS_DIR/$project/ $_VALUES || EXIT=1
      done
```

Не забываем в корень values.yml внести список Helm-чартов, которые Вы планируете развернуть в кластере:

```yaml
tags:
  auth: true
  comments: true
  publish: true
```

Содержимое hse/charts/Chart.yml файла:

```yaml
---
apiVersion: v1
description: A Helm chart for Kubernetes
name: hse
version: 1.0.0

dependencies:
  - name: auth-HelmChart
    repository: alias:local
    version: 0.1.0
    tags:
      - auth
```

Структура директорий в Вашем репозитории должна выглядеть примерно так:

hse/charts/auth
hse/charts/Chart.yml
hse/values.yaml

Во вложенном подкаталогне hse/charts/... должны храниться наши чарты.